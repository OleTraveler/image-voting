package com.ot

package object lib {

  sealed trait Event
  case class InboundText(payload: String, fromNumber: String, toNumber: String) extends Event
  case class InboundMedia(payload: String, fromNumber: String, toNumber: String) extends Event


}
