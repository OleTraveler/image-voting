package lib

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import org.scalatest.FunSuite

import scala.concurrent.{Await, ExecutionContextExecutor}
import scala.concurrent.duration._

class DropboxTest extends FunSuite {

  implicit val testSystem: ActorSystem = ActorSystem()
  implicit val testExecutor: ExecutionContextExecutor = testSystem.dispatcher
  implicit val testMaterializer: ActorMaterializer = ActorMaterializer()


  val dropbox = new Dropbox {
    override def accessToken: String = ""

    override implicit val system = testSystem
    override implicit val executor: ExecutionContextExecutor = testExecutor
    override implicit val materializer: ActorMaterializer = testMaterializer
  }

  ignore("can load url to dropbox") {

    val request = dropbox.saveImage("test.jpg", "https://upload.wikimedia.org/wikipedia/commons/2/2a/Hummingbird.jpg")

    val result = Await.result(request, Duration(5, SECONDS))

    println(result)
  }

  test("list files in dropbox") {

    val result = Await.result(dropbox.listImages, Duration(5,SECONDS))

    println(s"Result data ${result}")

  }

}
