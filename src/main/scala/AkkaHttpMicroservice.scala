import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.unmarshalling.Unmarshaller
import akka.stream.{ActorMaterializer, Materializer}
import com.typesafe.config.{Config, ConfigFactory}
import spray.json.DefaultJsonProtocol

import scala.concurrent.ExecutionContextExecutor

import com.ot.lib._


trait Service {
  implicit val system: ActorSystem
  implicit def executor: ExecutionContextExecutor
  implicit val materializer: Materializer

  def config: Config
  def dboxConfig: Config
  val logger: LoggingAdapter

  /** Unmarshall to one of the Event subclasses. */
  implicit val um:Unmarshaller[HttpEntity, Option[Event]] = {

    object InboundTextProtocol extends DefaultJsonProtocol {
      implicit val colorFormat = jsonFormat3(InboundText)
    }
    object InboundMessageProtocol extends DefaultJsonProtocol {
      implicit val colorFormat = jsonFormat3(InboundMedia)
    }

    import spray.json._


    Unmarshaller.byteStringUnmarshaller.mapWithCharset { (data, charset) => {
        val json = data.decodeString(charset.value).parseJson
        json.asJsObject.fields.get("type").flatMap {
          case JsString(str) => str match {
            case "inboundText" => {
              import InboundTextProtocol._
              Some(json.convertTo[InboundText])
            }
            case "inboundMedia" => {
              import InboundMessageProtocol._
              Some(json.convertTo[InboundMedia])
            }
            case _ => None //we only care about inboundText and inboundMedia
          }
          case _ => None
        }
      }
    }
  }


  val routes: Route = {
    logRequestResult("akka-http-microservice") {

      (post & path("events") & entity(as[Option[Event]])) { event =>
        complete(s"Recieved ${event.toString}")
      }
    }
  }
}

object AkkaHttpMicroservice extends App with Service {
  override implicit val system = ActorSystem()
  override implicit val executor = system.dispatcher
  override implicit val materializer = ActorMaterializer()

  override val config = ConfigFactory.load()
  override val dboxConfig = ConfigFactory.load("dbox.conf")
  override val logger = Logging(system, getClass)

  Http().bindAndHandle(routes, config.getString("http.interface"), config.getInt("http.port"))
}
