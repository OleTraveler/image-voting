package lib

import akka.http.javadsl.model.headers.HttpCredentials
import akka.http.scaladsl.unmarshalling._
import akka.http.scaladsl.marshalling._
import akka.http.scaladsl.marshalling.PredefinedToEntityMarshallers._
import akka.util.ByteString
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import HttpMethods._
import akka.stream.{Materializer, ActorMaterializer}
import akka.stream.scaladsl._
import com.ot.lib.{InboundMedia, InboundText, Event}
import lib.Dropbox.{ContentMetadata, DirectoryMetadata}
import spray.json.DefaultJsonProtocol

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}


object Dropbox {

  case class ContentMetadata(path: String, mimeType: String)
  case class DirectoryMetadata(hash: String, contents: Seq[ContentMetadata])

}
trait Dropbox {

  def accessToken: String

  implicit def system: ActorSystem
  implicit def executor: ExecutionContextExecutor
  implicit def materializer: Materializer

  implicit final def toResponseMarshaller[A](implicit ec: ExecutionContext): ToResponseMarshaller[Source[String, A]] =
    Marshaller.withFixedCharset(MediaTypes.`text/plain`, HttpCharsets.`UTF-8`) { messages =>
      val data = messages.map(ByteString(_))
      HttpResponse(entity = HttpEntity.CloseDelimited(MediaTypes.`text/plain`, data))
    }

  val urlHeaders = List(headers.Authorization(HttpCredentials.createOAuth2BearerToken(accessToken)))

  def saveImage(name: String, url: String) : Future[HttpResponse] = {

    val multipartForm = Multipart.FormData(
      Source(List(
        Multipart.FormData.BodyPart.Strict("url", HttpEntity(ByteString(url)))
      ))
    )

    for {
      entity <- Marshal(multipartForm).to[RequestEntity]
      response <- Http().singleRequest(HttpRequest(POST, s"https://api.dropbox.com/1/save_url/auto/images/${name}", urlHeaders, entity))
    } yield response
  }

  implicit val um:Unmarshaller[HttpEntity, Option[DirectoryMetadata]] = {
    object ContentProtocol extends DefaultJsonProtocol {
      implicit val contentFormat = jsonFormat2(ContentMetadata)
    }
    import spray.json._

    import ContentProtocol._

    Unmarshaller.byteStringUnmarshaller.mapWithCharset { (data, charset) => {
      val json = data.decodeString(charset.value).parseJson.asJsObject

      for {
        hash <- json.getFields("hash").map{case JsString(value) => value}.headOption
      } yield {
        val contents = json.getFields("contents").flatMap{ case JsArray(items) => items }
          .map(x => x.convertTo[ContentMetadata])
        DirectoryMetadata(hash, contents)
      }
    }}
  }

  def listImages() : Future[Option[DirectoryMetadata]] = {

    for {
      get <- Http().singleRequest(HttpRequest(GET, s"https://api.dropboxapi.com/1/metadata/auto/images", urlHeaders))
      md <- Unmarshal(get.entity).to[Option[DirectoryMetadata]]
    } yield {
      md
    }


  }
}
